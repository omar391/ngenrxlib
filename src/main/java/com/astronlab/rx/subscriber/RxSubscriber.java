package com.astronlab.rx.subscriber;

import rx.Subscriber;

/**
 * Created by Omar on 1/18/2016.
 */
public abstract class RxSubscriber<T> extends Subscriber {

	public abstract void onCompleted();

	public void onError(Throwable e) {
		e.printStackTrace();
	}

	public abstract void onNext(Object result);
}
