package com.astronlab.rx.sample;

import com.astronlab.rx.observable.RxObservable;
import com.astronlab.rx.thread.RxThreadScheduler;

/**
 * Created by Omar on 2/5/2016.
 */
public class AsyncThread {

    public static void main(String[] args) {
        AsyncThread asyncThread = new AsyncThread();
        asyncThread.test();
    }

    void test() {
        MySubscriber subscriber = new MySubscriber();

        //Static way without on subscribe
        RxObservable.just("Ops mannn..it's static-2").observeOn(RxThreadScheduler.instance()).subscribe(subscriber);
        
        //TODO: implement threaded scheduler
    }
}
