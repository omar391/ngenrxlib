package com.astronlab.rx.sample;

import com.astronlab.rx.subscriber.RxSubscriber;

public class MySubscriber<T> extends RxSubscriber {

	@Override public void onCompleted() {
		System.out.println("Just completed");
	}

	@Override public void onNext(Object o) {
		System.out.println("Subscription result arrived: "+o.toString());
	}
}