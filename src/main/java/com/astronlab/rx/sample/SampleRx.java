package com.astronlab.rx.sample;

import com.astronlab.rx.observable.RxObservable;
import com.astronlab.rx.observer.RxOnSubscribe;
import com.astronlab.rx.subscriber.RxSubscriber;
import rx.observers.SafeSubscriber;

/**
 * Created by Omar on 1/18/2016.
 */
public class SampleRx implements RxOnSubscribe {
	RxObservable rxObservable = new RxObservable(this);

	public static void main(String[] args) {
		SampleRx sampleRx = new SampleRx();
		sampleRx.test();
	}

	void test() {
		MySubscriber subscriber = new MySubscriber();

		//On subscribe way
		rxObservable.subscribe(subscriber);

		//Static way without on subscribe
		RxObservable.just("Ops mannn..it's static-2").subscribe(subscriber);
	}

	@Override public void call(Object o) {
		System.out.println("Just subscribed-1");
		((SafeSubscriber)o).onNext("Yahoooo!!-1");
	}
}
