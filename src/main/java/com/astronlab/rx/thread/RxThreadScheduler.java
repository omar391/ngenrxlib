package com.astronlab.rx.thread;

import rx.Scheduler;
import rx.internal.schedulers.NewThreadWorker;
import rx.internal.util.RxThreadFactory;

public class RxThreadScheduler extends Scheduler {


    private static String THREAD_NAME_PREFIX = "RxNewThreadScheduler-";
    private final RxThreadFactory THREAD_FACTORY = new RxThreadFactory(THREAD_NAME_PREFIX);
    private static final RxThreadScheduler INSTANCE = new RxThreadScheduler();

    public static RxThreadScheduler instance() {
        return INSTANCE;
    }

    public void setThreadNamePrefix(String name) {
        THREAD_NAME_PREFIX = name + "-";
    }

    private RxThreadScheduler() {

    }

    @Override
    public Worker createWorker() {
        return new NewThreadWorker(THREAD_FACTORY);
    }
}