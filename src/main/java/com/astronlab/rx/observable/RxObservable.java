package com.astronlab.rx.observable;

import com.astronlab.rx.observer.RxOnSubscribe;
import rx.Observable;
import rx.Subscriber;

/**
 * Created by Omar on 1/18/2016.
 */
public class RxObservable<T> extends Observable{
	/**
	 * Creates an Observable with a Function to execute when it is subscribed to.
	 * <p>
	 * <em>Note:</em> Use {@link #create(OnSubscribe)} to create an Observable, instead of this constructor,
	 * unless you specifically have a need for inheritance.
	 *
	 * @param f {@link OnSubscribe} to be executed when {@link #subscribe(Subscriber)} is called
	 */
	public RxObservable(RxOnSubscribe<T> f) {
		super(f);
	}

}
