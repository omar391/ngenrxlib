package com.astronlab.rx.observer;

import rx.Observable;

/**
 * Created by Omar on 1/18/2016.
 */
public interface RxOnSubscribe<T> extends Observable.OnSubscribe<T> {
	//This interface is hardly required unless you want to prompt event from onSubscribe
	//Implements methods from Action1 interface
	//@Override public void call(Object o);
}
